import React,{useState} from 'react'
import './Header.css'
import Menu from '../menu/Menu'
import Search from '../search/Search'
import Hamburger from '../hamburger/Hamburger'
const Header = () => {
    const [menu,setMenu]=useState(false)
    const showMenu=()=>{
        setMenu(!menu)
    }
    console.log(menu);
    return ( 
        <div className="site-header container-fluid">
            <div className="logo">
                <img src="logo.png" alt="logo"/>
            </div>

            <Menu menu={menu}/>
            <Search/>
            <Hamburger showMenu={showMenu} menu={menu}/>
        </div>
        
     );
}
 
export default Header;