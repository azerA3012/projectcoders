import React, { Component } from "react";
import "./Category.css";
import { connect } from "react-redux";

class CategoryList extends Component {
  
  // selectedCat = (event)=>{
    
  //   this.setState({active: event.target.innerText});
    
  // }
  
  render() {
    return (

      <div className="category">
        <ul className="category-menu">
          
          {this.props.categories.map((category, index) => {
            return (
              <li key={index} className="category-item">
                <a
                  key={index}
                  // href="#"
                  className="category-link"
                  onClick={(event)=>this.props.onCategoryChange(category.name)}
                >
                  {category.name}
                  
                </a>
              </li>
            );
          })}
        </ul>
      </div>
    );
  }
}


const mapStateToProps = (state) => {
  return {
    categories: state.categories,
    products: state.products,
    activeCat:state.active,
    changePrice:state.price,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    onCategoryChange: (name) =>
      dispatch({ type: "CHANGE_ACTIVE_CAT", payload: name }),
  };
};

export default connect(mapStateToProps,mapDispatchToProps)(CategoryList);
