import React, { Component } from "react";
import "./Filter.css";
import { connect } from "react-redux";

class Filter extends Component {
    
  render() {
      
    return (
      <div className="filter">
        <input
          className=" col-md-8 "
          type="range"
          name=""
          id=""
          min="0"
          max="500"
          onChange={(event)=>this.props.onPriceChange(Number(event.target.value))}
        />
        <span className="price">price 0-{this.props.changePrice}</span>
        
      </div>
    );
  }
}

const mapStateToProps = (state) => {
    return {
      categories: state.categories,
      products: state.products,
      activeCat:state.active,
      changePrice:state.price
    };
  };

  const mapDispatchToProps = (dispatch) => {
    return {
      onPriceChange: (name) =>
        dispatch({ type: "PRICE", payload: name }),
    };
  };

export default connect(mapStateToProps,mapDispatchToProps)(Filter);
