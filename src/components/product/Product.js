import React, { Component } from "react";
import "./Product.css";
import { connect } from "react-redux";

class Product extends Component {
 
  render() {
    return (
      <div className="container">
        <div className="row">
          {this.props.products.map((product, index) => {
            
            
            if(('bütün mallar'==this.props.activeCat || "all"==this.props.activeCat) && product.price<=this.props.changePrice ){
              return (
                <div key={index} className="col-md-4 col-md-4 col-sm-4 col-sm-10">
                  <div
                    key={product.id}
                    className="product"
                    style={{ backgroundImage: `url(${product.img})` }}
                  >
                    <div className="info">
                      <h3 className="product-category">{product.category}</h3>
                      <p className="product-name">{product.name}</p>
                      <span className="product-price">
                        {product.price}&#8380;
                      </span>
                    </div>
                  </div>
                </div>
              );
            }
            else if (product.category==this.props.activeCat && product.price<=this.props.changePrice  ){
              return (
                <div key={index} className="col-md-4 col-md-4 col-sm-4 col-sm-10">
                  <div
                    key={product.id}
                    className="product"
                    style={{ backgroundImage: `url(${product.img})` }}
                  >
                    <div className="info">
                      <h3 className="product-category">{product.category}</h3>
                      <p className="product-name">{product.name}</p>
                      <span className="product-price">
                        {product.price}&#8380;
                        
                        
                      </span>
                    </div>
                  </div>
                </div>
                
              );
            }
           

           
           
            
            
          })}
        </div>
      </div>
    );
  }

 
}



const mapStateToProps = (state) => {
  return {
    products: state.products,
    categories:state.categories,
    activeCat:state.active,
    changePrice:state.price
  };
};

export default connect(mapStateToProps)(Product);
