import React from 'react'
import './Menu.css'
const Menu = (props) => {
    return ( 
        <div>
            <ul className="menu" style={props.menu?{display: "flex"}:null}>
                <li className="menu-item"><a href="#" className="menu-link">Ana Səhifə</a></li>
                <li className="menu-item"><a href="#" className="menu-link">Kateqoriyalar</a></li>
                <li className="menu-item"><a href="#" className="menu-link">Haqqımızda</a></li>
                <li className="menu-item"><a href="#" className="menu-link">Əlaqə</a></li>
            </ul>
        </div>
     );
}
 
export default Menu;