import React from "react";
import Header from "./components/header/Header";
import "./css-fontawesome/all.css";
import "./App.css";

import Carousell from "./components/carousel/Carousel";
import CategoryList from "./components/category/CategoryList";
import Product from "./components/product/Product";
import Filter from "./components/filter/Filter";

function App() {
  return (
    <div className="App">
      
      <Header />
      <Carousell />
      <div className="container-fluid media">
        <div className="Category">
          <h3>Kateqoriyalar</h3>
          <hr/>
          <CategoryList />
          <h3 className="filter-price">Qiymətə görə axtarış</h3>
          <hr/>
          <Filter/>
        </div>
        
        <Product/>
        
       
      </div>
    </div>
  );
}

export default App;
