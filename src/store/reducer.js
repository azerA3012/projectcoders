const initialState = {
  categories: [
    { name: "bütün mallar" },
    { name: "ət məhsulları" },
    { name: "süd məhsulları" },
    { name: "meyvə" },
    { name: "tərəvəz" },
    { name: "mürəbbə" },
    { name: "bitki və arı məhsulları" },
  ],
  active: "all",
  price:500,
  products: [
    {
      id: 0,
      category: "meyvə",
      name: "alma",
      price: 2,
      img: "product/apple.jpg",
    },
    {
      id: 1,
      category: "tərəvəz",
      name: "kələm",
      price: 1,
      img: "product/kelem.jpg",
    },
    {
      id: 2,
      category: "mürəbbə",
      name: "gilas mürəbbəsi",
      price: 5,
      img: "product/jam.jpg",
    },
    {
      id: 3,
      category: "ət məhsulları",
      name: "halal kəsim ət",
      price: 12,
      img: "product/meat.jpg",
    },
    {
      id: 4,
      category: "süd məhsulları",
      name: "süd",
      price: 1,
      img: "product/milk.jpg",
    },
    {
      id: 5,
      category: "bitki və arı məhsulları",
      name: "bal",
      price: 10,
      img: "product/honeyy.jpg",
    },
  ],
};

const reducer = (state = initialState, action) => {
  if (action.type === "CHANGE_ACTIVE_CAT") {
    return {
      ...state,
      active: action.payload,
    };
  }
  if(action.type==="PRICE"){
      return{
          ...state,
          price:action.payload
      }
  }
  return state;
};

export default reducer;
